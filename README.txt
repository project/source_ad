----------------------------------------------------------------------------------------------------------------
                                                Source Ad
----------------------------------------------------------------------------------------------------------------


OVERVIEW
-----------------

The module allows you to place advertisement on your site in several unusual ways, such as:

1) Source code of site pages as HTML comment

For example, job ad for frontend developer:
<!--
=============================================================
You're curious about source code of the page, aren't you?
Take a look at our hot vacancy of Junior Frontend Developer
http://example.com/job/junior-frontend-developer
=============================================================
-->

2) HTTP Response headers

You can choose from Advertisement, Hacker and Read-Me headers.
For example, say hello to a hacker using Hacker header.
Let him know that you're watching for him :) He'll like it.

3) Browser Javascript console

For example, use browser console to attract Javascript developers to your job board.


The primary target audience is developers and web engineers.
They all like to learn source code of web sites, HTTP headers and messages in JS console.

Some more reasons to use:

- Advertise a hot vacancy
- Look for a development partner
- Sell some web tool
- Sell your website or domain
- Notify about code license
- Just say hello to colleagues


Sponsored by HookAny.com - Russian Drupal Job Board.


INSTALLATION
-----------------
Just enable the module and enjoy.


CONFIGURATION
-----------------
Go to admin/config/system/source_ad page, then enter and publish your ad.


AUTHORS & CONTRIBUTORS
-----------------
Author: Konstantin Komelin https://drupal.org/user/1195752
Contributors: Thanks to all guys who helped https://drupal.org/node/2171283/committers

